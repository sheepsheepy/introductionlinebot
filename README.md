# IntroductionLineBot

## 環境 : Line + Heroku + PHP
##### 架設教學實在很多...語言也很多...所以就不多贅述了...XD
- [Line Developers](https://developers.line.biz/en/)  
- [Heroku](https://dashboard.heroku.com/apps)  
- [PHP](https://github.com/CHY0919/linebot-php-heroku-framework)   

###### 可以打的關鍵字: 

- 程式語言,程式,coding,computer language,computerlanguage
- c++ 
- hello,hi,你好,嗨,說話,跟我說話
- 自我介紹,自介,關於我,Angel
- opencv
- java
- app,java app,javaapp
- compiler,編譯
- dft
- sobel
- 中央氣象局,acid rain or not,opendata,acid rain,acid,acidrain
- html,html/css,css,php,machine,mechine learning,protein,amino acid,molecular biology,生物資訊學

## Code: index.php
 - LineBotTiny.php 是原本就在line官方的package裡面
 - json 資料夾裡面為回覆訊息，使用讀檔的讀取來回覆

#### caseFollow( )
這個是當bot被加入好友時做的處理。
````php
function caseFollow($client, $event)
{
	error_log("被加入好友");
	$client->replyMessage(
		array(
		'replyToken' => $event['replyToken'],
		'messages' => 
		array
			(
				array
				(
					'type' => 'text',
					'text' => '你好，我是一個介紹Angel的LineBot。請輸入「關於我」來獲得更多資訊吧!'
				),
			)
		)
	);		
}
````

#### caseMessage( )
收到訊息的時候的判斷，分別用另外兩個function搜尋關鍵字回傳值判斷。

````php
function caseMessage($client, $event)
{
	$message = $event['message'];
	switch ($message['type']) 
	{
		case 'text':
			$m_message = $message['text'];
			$m_message = strtolower($m_message);
			if($m_message!="")
			{
				if(is_null($keyline = FindinGitJson($m_message)) != TRUE)
				{
					$client->replyMessage
					(
						array
						(
							'replyToken' => $event['replyToken'],
							'messages' => ReplyGitMessage($keyline)
							
						)
					);
				}
				else
				{
					if(is_null($textline = FindinTextJson($m_message))!= TRUE)
					{
						$client->replyMessage
						(
							array
							(
								'replyToken' => $event['replyToken'],
								'messages' => ReplyTextMessage($textline)
							)
						);
					}
					else
					{
						$client->replyMessage
						(
							array
							(
								'replyToken' => $event['replyToken'],
								'messages' => ReplyUnfindMessage("對不起我沒有這方面的資訊。請輸入以下關鍵字再試試看: C++ / Java / dft")
							)
						);
					}
				}
			}
			break;
		
	}
}
````

#### FindinTextJson( )
從textreply.json裡面搜尋&獲取關鍵字，之後放到$textline的array裡。
````php
function FindinTextJson($message)
{
	$json_reply = file_get_contents("./json/textreply.json"); 
	$json_data = json_decode($json_reply, true);
	foreach($json_data as $data)
	{
		$keyword = explode(",",$data["keyword"]);// cut keyword by ,
		foreach($keyword as $key)// scan every keyword
		{
			if(strcmp($message,$key) == 0)
			{
				$textline = 
				array
				(
					"keyword" => $data["keyword"],
					"replya" => $data["replya"],
					"replyb" => $data["replyb"],
					"replyc" => $data["replyc"]
				);
				return $textline;
			}
			
		}
	}
	return NULL;
}
````
#### FindinGitJson( )
從gitreply.json裡面搜尋&獲取關鍵字，之後放到$keyline的array裡。
````php
function FindinGitJson($message)
{
	$json_reply = file_get_contents("./json/gitreply.json"); 
	$json_data = json_decode($json_reply, true);
	foreach($json_data as $data)
	{
		$keyword = explode(",",$data["keyword"]);// cut keyword by ,
		foreach($keyword as $key)// scan every keyword
		{
			if(strcmp($message,$key) == 0)
			{
				$keyline = 
				array
				(
					"title" => $data["title"],
					"photourl" => $data["photourl"],
					"text" => $data["text"],
					"url"=>$data["url"],
					"keyword"=>$data["keyword"],
					"more"=>$data["more"],
				);
				return $keyline;
			}
			
		}
	}
	return NULL;
}
````
#### ReplyTextMessage()
從FindinTextJson()裡面得到回傳值，傳進來放入line需要的格式。

````php
function ReplyTextMessage($textline)
{	
	return 
	array
	(
		array
		(
			'type' => 'text',
			'text' => $textline["replya"]
		),
		array
		(
			'type' => 'text',
			'text' => $textline["replyb"]
		),
		array
		(
			'type' => 'text',
			'text' => $textline["replyc"]
		),
	);
	
}
````

#### ReplyGitMessage()
從FindinGitJson()裡面得到回傳值，傳進來放入line需要的格式。
````php
function ReplyGitMessage($keyline)
{	
	error_log("準備回傳旋轉木馬訊息");
	return
	array
	(
		array
		(
			'type' => 'template', 
			'altText' => $keyline["title"], 
			'template' => 
			array
			(
				'type' => 'buttons',
				'thumbnailImageUrl' => $keyline["photourl"], 
				'title' => $keyline["title"], 
				'text' => $keyline["text"], 
				'actions' => 
				array
				(
					array
					(
						'type' => 'uri', // 類型 (連結)
						'label' => '在Git中察看', // 標籤 3
						'uri' => $keyline["url"] // 連結網址
					)
				
				)
			)
		),
		array
		(
			'type' => 'text',
			'text' => $keyline["more"],
		),
	);
}
````
#### ReplyUnfindMessage()
沒有在這兩個file裡面搜尋到的東西，會回傳找不到，一樣放回來line需要的格式回傳。
````php
function ReplyUnfindMessage($inputStr)
{	
	settype($inputStr, "string");
	error_log("訊息【".$inputStr."】準備以文字訊息回傳");
	
	return 
	array
	(
		array
		(
			'type' => 'text',
			'text' => $inputStr,
		),
	);
	
}
````
## 心得 & 以後注意點

1. 卡住的地方很多，一開始是架環境  
	因為一開始看得是Node.js的教學，所以package完全下錯。Heroku的偵測語言也不對。  
	原本想用Google Cloud Server當天晚上卻當場炸掉給我看。-> 這提示了我說，它非常不穩定。  
	改回來想法去找php+Heroku+Line的教學之後主要參考這兩個:  
	[骰子狗：開放原始碼的LINE骰子機器人](https://github.com/retsnimle/TrpgLineBot-php)   
	[美食指南(LINE Bot範例)](https://github.com/jarsing/linebot-foodguide)   
	但是放上去又不能跑，很崩潰阿。不知道是我當時的環境還沒架好還是他們的code跟我不符合。   
	之後就找到了這個 [linebot-php-heroku-framework] (https://github.com/CHY0919/linebot-php-heroku-framework) 
	安裝好終於可以開始寫了。   

2. Json:   
	網路上的Json格式不知道為什麼google sheet的樣本很多。
	我自己是看過Json的格式但是實在不知道為什麼google sheet看起來長得跟我不一樣。Orz
	後來乾脆自己手動把它改成我想要的格式了。(反正也讀的了=A=)
	因為Json在php上讀取的方式也很多，所以也多試了幾種。
	※之後要注意以後手動改格式的時候要記的逗點阿....

3. line Array的格式    
 	因為line的Bot回傳文字的時候需要固定的array格式，所以很容易少一個多一個/少一個括號多一個括號。
	大部分的問題要仔細一點，這時候這個網站就非常需要了
	[line-example-bot-tiny-php](https://github.com/GoneTone/line-example-bot-tiny-php)
	它完全整理了官方的用法。(大神跪拜)

4. Debug的部分    
	我會建議說，一開始寫的時候讓你的應聲蟲回傳留著，先不要刪掉它。
	如果再測試的時候你的應聲蟲回傳ok，那就可能不是foreach那邊的問題。
	如果不ok那表示問題到他完全無法執行了....
	可以裝個xampp apache debug下(蠻好用的)。

5. git & cmd 指令     
	基本上heroku上面都有寫正確的指令，但是還是記得要先登錄r。

大概就這樣吧。為期一個星期的linebot結束。如果之後有機會再幫我玩的遊戲做個random抽抽bot之類的(?

## Reference (A lot!!)

[linebot - npm](https://www.npmjs.com/package/linebot)  
[LINE Bot聊天機器人程式開發教學 系列文](https://swf.com.tw/?p=1091)  
[Chatbot 開發指南：使用 LINE Bot PHP SDK 打造問答型聊天機器人](https://www.appcoda.com.tw/line-chatbot-sdk/)  
[用 PHP 實現 Line Message API 接收系統訊息](https://blog.toright.com/posts/5727/%E7%94%A8-php-%E5%AF%A6%E7%8F%BE-line-message-api-%E6%8E%A5%E6%94%B6%E7%B3%BB%E7%B5%B1%E8%A8%8A%E6%81%AF.html)  
[使用PHP+Heroku快速打造Linebot回話機器人](http://www.chy.tw/2017/08/phpherokulinebot.html)  
[linebot-php-heroku-framework](https://github.com/CHY0919/linebot-php-heroku-framework) 
[linebot-foodguide](https://github.com/jarsing/linebot-foodguide)  
[骰子狗：開放原始碼的LINE骰子機器人](https://github.com/retsnimle/TrpgLineBot-php)    
[GoneTone/line-example-bot-tiny-php](https://github.com/GoneTone/line-example-bot-tiny-php)  
 ... And 還有更多的 Google / StackOverflow / Line官方文件 / Heroku 文件 ...

--------------
## linebot-php-heroku-framework

a tiny PHP framework to deploy your linebot on heroku

just comebine the [heroku/php-getting-started](https://github.com/heroku/php-getting-started) and [linebot-php-sdk](https://github.com/line/line-bot-sdk-php)

### Getting Started

1.Register linebot service [lineservice](https://business.line.me/services/bot).

2.Turn on the [bot service](https://admin-official.line.me/) .

3.Enable Webhook sending and disable auto-sending setting.

4.Remember your ChannelSecret and issued ChannelAccessToken as parameters.

5.Deploy your linebot with the under button.

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

6.Fill Webhook URL in developers page with 	https://[your app name].herokuapp.com:443/callback

7.Done.

See detail with picture at [my web](http://www.chy.tw/2017/08/phpherokulinebot.html).

### License

the original code owned by LINE Corporation


```
Copyright 2016 LINE Corporation

LINE Corporation licenses this file to you under the Apache License,
version 2.0 (the "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at:

  https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations
under the License.
```


